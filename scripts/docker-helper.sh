#!/usr/bin/env bash
# Docker helper for building/pushing images
# script by gitlab.com/hhromic

set -Eeuo pipefail
shopt -s nullglob

# prefix for image names
NAME_PREFIX=${NAME_PREFIX:-solarus}

# version to use for images
IMAGE_VERSION=${IMAGE_VERSION:-latest}

# version to use for base images
BASE_IMAGE_VERSION=${BASE_IMAGE_VERSION:-$IMAGE_VERSION}

# output directory for package lists
PKGLISTS_DIR=${PKGLISTS_DIR:-pkglists}

# operation to perform
[[ $# -gt 0 ]] && OPERATION=$1
if [[ -z ${OPERATION:-} ]]; then
  printf "usage: %s [build|push|pull|tag-latest|pkglist] <IMAGE_DIR> ...\n" "$0"
  printf "note: all known image directories will be used if none are provided\n"
  exit 1
fi

# image directories to process
IMAGE_DIRS=({base,linux,mingw,docs}-build-env utilities-env)
[[ $# -gt 1 ]] && IMAGE_DIRS=("${@:2}")

# iterate image directories and perform requested operation
for image_dir in "${IMAGE_DIRS[@]}"; do
  image_dir=${image_dir%*/}
  image=$NAME_PREFIX/$image_dir
  case $OPERATION in
    build)
      printf "Building image '%s' ...\n" "$image:$IMAGE_VERSION"
      docker build \
        --build-arg BASE_IMAGE_VERSION="$BASE_IMAGE_VERSION" \
        --build-arg IMAGE_VERSION="$IMAGE_VERSION" \
        --squash --tag "$image:$IMAGE_VERSION" "$image_dir"
      ;;
    push)
      printf "Pushing image '%s' ...\n" "$image:$IMAGE_VERSION"
      docker push "$image:$IMAGE_VERSION"
      ;;
    pull)
      printf "Pulling image '%s' ...\n" "$image:$IMAGE_VERSION"
      docker pull "$image:$IMAGE_VERSION"
      ;;
    tag-latest)
      printf "Tagging image '%s' as 'latest' ...\n" "$image:$IMAGE_VERSION"
      docker tag "$image:$IMAGE_VERSION" "$image:latest"
      ;;
    pkglist)
      printf "Generating package list for '%s' ...\n" "$image:$IMAGE_VERSION"
      mkdir -p "$PKGLISTS_DIR"
      IFS=" " read -r -a pkglist_cmd <<< "$(docker inspect "$image:$IMAGE_VERSION" \
          --format '{{index .Config.Labels "org.solarus-games.pkglist-cmd"}}')"
      docker run --rm "$image:$IMAGE_VERSION" "${pkglist_cmd[@]}" 2>/dev/null \
          | sort > "$PKGLISTS_DIR/$image_dir"
      ;;
    *)
      printf "error: operation '%s' unknown\n" "$OPERATION"
      exit 1
      ;;
  esac
done
