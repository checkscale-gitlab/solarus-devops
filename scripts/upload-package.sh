#!/usr/bin/env bash
# Helper script for uploading packages to the GitLab package registry
# script by gitlab.com/hhromic

set -Eeuo pipefail

declare -r GITLAB_API_BASE=https://gitlab.com/api/v4
declare -r PROJECTS_API=$GITLAB_API_BASE/projects

if [[ $# -lt 4 ]]; then
  cat << __EOF__
Usage: $0 GITLAB_PROJECT PACKAGE_NAME PACKAGE_VERSION PACKAGE_FILE

Positional arguments:

    GITLAB_PROJECT     ID or URL-encoded path of the GitLab project
    PACKAGE_NAME       Package name for publishing to the package registry
    PACKAGE_VERSION    Package version for publishing to the package registry
    PACKAGE_FILE       Local package file to upload to the package registry

Environment variables:

    GITLAB_TOKEN       GitLab personal access token with at least 'api' scope.

__EOF__
  exit 0
fi

if [[ -z ${GITLAB_TOKEN:-} ]]; then
  printf "error: GITLAB_TOKEN environment variable is missing\n"
  exit 1
fi

printf "Uploading package '%s' for project '%s' to GitLab ...\n" "$4" "$1"
response=$(curl \
  --header "Private-Token: $GITLAB_TOKEN" \
  --upload-file "$4" \
  "$PROJECTS_API/$1/packages/generic/$2/$3/${4##*/}")
printf "Response: %s\n" "$response"
