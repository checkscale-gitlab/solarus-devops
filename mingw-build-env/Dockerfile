ARG BASE_IMAGE_VERSION=latest
FROM solarus/base-build-env:${BASE_IMAGE_VERSION}

# Image labels
ARG IMAGE_VERSION=latest
LABEL description="Solarus MinGW build environment image" \
      maintainer="The Solarus Team" \
      version="${IMAGE_VERSION}" \
      org.solarus-games.pkglist-cmd="pacman -Q"

# Synchronise pacman repositories and update system using yay
RUN sudo -u yay -- yay --noconfirm -Syu

# Install required mingw-w64 packages using yay
RUN sudo -u yay -- yay --noconfirm --needed -S \
    mingw-w64-gcc mingw-w64-cmake mingw-w64-pkg-config \
    mingw-w64-sdl2 mingw-w64-sdl2_image mingw-w64-sdl2_ttf \
    mingw-w64-openal mingw-w64-libvorbis mingw-w64-libmodplug \
    mingw-w64-glm mingw-w64-physfs mingw-w64-lua51 mingw-w64-luajit-2.1 \
    mingw-w64-qt5-base mingw-w64-qt5-tools

# Install newline conversion tools for text files
RUN sudo -u yay -- yay --noconfirm --needed -S \
    dos2unix

# Copy MinGW packaging and helper scripts
COPY mingw_find_deps.sh mingw_make_pkg.sh pkg_add.sh \
     /usr/local/bin/

# Clear yay cache
RUN sudo -u yay -- rm -rf /home/yay/.cache/*

# Remove unnecessary data from the image
RUN set -e; \
    for prefix in /usr/{,{i686,x86_64}-w64-mingw32}/share; do \
        rm -rf ${prefix}/{doc,gtk-doc,man,info,texinfo,locale,i18n,guile,ibus,perl5}/*; \
    done
