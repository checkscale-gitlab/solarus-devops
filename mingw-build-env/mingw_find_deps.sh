#!/usr/bin/env bash
# Find runtime dependencies of a binary object built with MinGW.
# script by gitlab.com/hhromic
#
# Usage: $0 <ARCH> <BINARY_OBJ>
#        ARCH is the MinGW architecture, i.e. "i686" or "x86_64"
#        BINARY_OBJ is the binary object to process

set -Eeuo pipefail

declare -g -r MINGW="$1"-w64-mingw32
declare -g -A INSTALLED
declare -g -a SCANNED

function get_name() {
  "$MINGW"-objdump -p "$1" 2>/dev/null | awk '/^Name/{print $NF}'
}

function get_deps() {
  "$MINGW"-strings "$1" | grep -iE '^([^\ ])*\.dll$'
}

function find_installed() {
  local lib
  while IFS= read -r -d '' lib; do
    INSTALLED[$(get_name "$lib")]=$lib
  done < <(find /usr/"$MINGW" -name "*.dll" -print0)
}

function is_scanned() {
  local item
  for item in "${SCANNED[@]}"; do
    [[ $item = "$1" ]] && return 0
  done
  return 1
}

function find_deps() {
  local name
  local dep
  name=$(get_name "$1")
  [[ $2 -ne 0 && -n $name ]] && printf "%s\t%s\n" "$1" "$name"
  for dep in $(get_deps "$1"); do
    is_scanned "$dep" && continue
    SCANNED+=("$dep")
    if [[ ${#INSTALLED[$dep]} -ne 0 ]]; then
      find_deps "${INSTALLED[$dep]}" 1
    fi
  done
}

find_installed
find_deps "$2" 0
