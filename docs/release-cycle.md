# Solarus Release Cycle

This document describes the *Solarus Release Cycle* for correctly creating a
release using the GitLab CI/CD configurations in this repository.

The Solarus projects required for creating a release are:

* [Solarus Engine](https://gitlab.com/solarus-games/solarus)
* [Solarus Quest Editor](https://gitlab.com/solarus-games/solarus-quest-editor)
* [Solarus Sample Quest](https://gitlab.com/solarus-games/solarus-sample-quest)
* [Solarus Documentation](https://gitlab.com/solarus-games/solarus-doc)

## Table of Contents

* [Repository Dependencies](#repository-dependencies)
* [Creating a Release](#creating-a-release)
* [Release Artifacts](#release-artifacts)
* [Uploading Release Packages](#uploading-release-packages)
* [Release Checklist](#release-checklist)

## Repository Dependencies

The following table is a summary of Solarus project repository dependencies as
defined in the GitLab CI/CD configurations.

| Repository             | CI/CD Stage | Dependency             | Branch      |
|:-----------------------|:------------|:-----------------------|:------------|
| `solarus`              |             | *no dependencies*      |             |
| `solarus-doc`          |             | *no dependencies*      |             |
| `solarus-quest-editor` | `build`     | `solarus`              | *tag*/`dev` |
| `solarus-quest-editor` | `package`   | `solarus-sample-quest` | *tag*/`dev` |
| `solarus-quest-editor` | `package`   | `solarus-doc`          | *tag*/`dev` |
| *quests*               | `test`      | `solarus`              | *var*/`dev` |

The *Branch* column indicates the branch in the dependency repository that is
used by the CI/CD jobs to obtain the artifacts:

* *var* refers to a specific branch or tag name configured in the
  [GitLab CI/CD Variables UI](https://docs.gitlab.com/ee/ci/variables/#via-the-ui)
* *tag* refers to the current tag being processed by the CI/CD pipeline
* `dev` is the branch of the same name used when no *var* is defined or no
  *tag* is being processed by the CI/CD pipeline

> **Important:** For the *quests* repositories, the CI/CD variable name for the
branch or tag to use for testing the quest is `SOLARUS_REF`. It can be defined
to a specific Solarus Engine version to use instead of `dev`. The chosen
release tag must have a completed CI/CD pipeline.

## Creating a Release

> **Reminder:** All Solarus projects follow the
[Git-Flow Model](https://nvie.com/posts/a-successful-git-branching-model/).

In each Solarus project, a *release* is designated with a *tag*. However, due
to project dependencies, release tags need to be created in each repository in
a specific order and after certain pipelines have finished.

The following are the required steps for creating a complete Solarus Release:

> **Important:** The format for the release tags is `vX.Y.Z`, e.g. `v1.6.3`.
All the required Solarus projects must be tagged with the same release version
number, regardless if they have changed or not.

1. Make sure that all the required Solarus projects are ready for a release
   - Check that their CI/CD pipelines are passing
   - Check that their changelogs (if any) are up-to-date
   - Check that the version number in the source code or CMake is correct
   - Check that there are no pending issues for the release milestone
2. Prepare a release branch in each required repository
   - Create a new `release-X.Y.Z` branch from `dev`, e.g. `release-1.6.3`
   - Merge the new `release-X.Y.Z` branch into `master`
3. Create a new *tag* from `master` in the **Solarus Engine** repository
   - Wait for the newly created *tag* pipeline to finish
4. Create a new *tag* from `master` in the **Solarus Sample Quest** repository
   - Wait for the newly created *tag* pipeline to finish
5. Create a new *tag* from `master` in the **Solarus Documentation** repository
   - Wait for the newly created *tag* pipeline to finish
6. Create a new *tag* from `master` in the **Solarus Quest Editor** repository
7. Bump the version number in the changelog of the `dev` branch (if any) for
   all the above project repositories
8. *The release cycle is now complete!* :tada:

## Release Artifacts

After completing the release cycle, the Solarus Engine, Quest Editor and
Documentation repositories will all have available release artifacts:

* **Solarus Engine**
  - Windows x64 Package:

        https://gitlab.com/solarus-games/solarus/-/jobs/artifacts/
            vX.Y.Z/raw/solarus-player-x64-vX.Y.Z.zip?job=mingw-x64-package

  - Windows x86 Package:

        https://gitlab.com/solarus-games/solarus/-/jobs/artifacts/
            vX.Y.Z/raw/solarus-player-x86-vX.Y.Z.zip?job=mingw-x86-package

  - Windows x64 Portable Package:

        https://gitlab.com/solarus-games/solarus/-/jobs/artifacts/
            vX.Y.Z/raw/solarus-player-x64-portable-vX.Y.Z.zip?job=mingw-x64-portable-package

* **Solarus Quest Editor**
  - Windows x64 Package:

        https://gitlab.com/solarus-games/solarus-quest-editor/-/jobs/artifacts/
            vX.Y.Z/raw/solarus-x64-vX.Y.Z.zip?job=mingw-x64-package

  - Windows x86 Package:

        https://gitlab.com/solarus-games/solarus-quest-editor/-/jobs/artifacts/
            vX.Y.Z/raw/solarus-x86-vX.Y.Z.zip?job=mingw-x86-package

* **Solarus Documentation**
  - Quest Maker's Reference Manual (PDF)

        https://gitlab.com/solarus-games/solarus-doc/-/jobs/artifacts/
            vX.Y.Z/raw/build/latex/refman.pdf?job=docs-build

  - Quest Maker's Reference Manual (HTML)

        https://gitlab.com/solarus-games/solarus-doc/-/jobs/artifacts/
            vX.Y.Z/file/build/html/index.html?job=docs-build

## Uploading Release Packages

Some [release artifacts](#release-artifacts) must be uploaded as release packages
to the GitLab generic package registry for permanent hosting:

* [Solarus Engine](https://gitlab.com/solarus-games/solarus/-/pipelines?status=success)
  * Windows x64 Package
  * Mac OSX 64-bit Package (**not built by the CI/CD**)
* [Solarus Quest Editor](https://gitlab.com/solarus-games/solarus-quest-editor/-/pipelines?status=success)
  * Windows x64 Package
  * Mac OSX 64-bit Package (**not built by the CI/CD**)
* Quests
  * https://gitlab.com/solarus-games/zsdx/-/pipelines?status=success
  * https://gitlab.com/solarus-games/zsxd/-/pipelines?status=success
  * https://gitlab.com/solarus-games/zelda-roth-se/-/pipelines?status=success
  * https://gitlab.com/solarus-games/zelda-xd2-mercuris-chess/-/pipelines?status=success

A [Bash script is provided](../scripts/upload-package.sh) to facilitate uploading
these release artifacts to GitLab as packages.

To use the script, you must first
[obtain a GitLab personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#creating-a-personal-access-token)
with at least the `api` scope (this does not need to be done every time).

Then, export the token using the `GITLAB_TOKEN` environment variable before using the script:

    export GITLAB_TOKEN=my-gitlab-token

To upload release packages to the GitLab package registry, it is recommended to first
download all required release artifacts into a temporary directory. Then, the helper
script must be invoked using the following syntax:

     upload-package.sh GITLAB_PROJECT PACKAGE_NAME PACKAGE_VERSION PACKAGE_FILE

For convenience, parameter values for the Solarus packages are in the following table:

| `GITLAB_PROJECT` | `PACKAGE_NAME`             | `PACKAGE_VERSION` | `PACKAGE_FILE`                           |
|:-----------------|:---------------------------|:------------------|:-----------------------------------------|
| `6933824`        | `solarus`                  | `X.Y.Z`           | `solarus-player-x64-vX.Y.Z.zip`          |
| `6933848`        | `solarus-quest-editor`     | `X.Y.Z`           | `solarus-x64-vX.Y.Z.zip`                 |
| `6933865`        | `zsdx`                     | `X.Y.Z`           | `zsxd-X.Y.Z.solarus`                     |
| `6933868`        | `zsxd`                     | `X.Y.Z`           | `zsxd-X.Y.Z.solarus`                     |
| `6933862`        | `zelda-roth-se`            | `X.Y.Z`           | `zelda-roth-se-X.Y.Z.solarus`            |
| `6933864`        | `zelda-xd2-mercuris-chess` | `X.Y.Z`           | `zelda-xd2-mercuris-chess-X.Y.Z.solarus` |

For example, to upload a release package for the Solarus Quest Editor (64-bit) version 1.6.5:

     upload-package.sh 6933848 solarus-quest-editor 1.6.5 /path/to/solarus-x64-v1.6.5.zip

All published release packages for the Solarus Games group can be
[found here](https://gitlab.com/groups/solarus-games/-/packages).

## Release Checklist

Use the following checklist to make sure that everything is published and
up-to-date, including informing users and to not forget anything.

* Successfully complete [creating a release](#creating-a-release)
* Successfully complete [uploading release packages](#uploading-release-packages)
* Update the official website
  * Update download pages and links with released packages
  * Verify latest stable version number on the homepage
  * Write an announcement article if necessary
* Update the forum anti-spam configuration
  * The answer to the question *What is the latest release of Solarus?*
    should be updated (English and French forums)
* Publish an announcement on social networks about the release if necessary
  * Twitter
  * Facebook
  * Discord
