ARG BASE_IMAGE_VERSION=latest
FROM solarus/base-build-env:${BASE_IMAGE_VERSION}

# Image labels
ARG IMAGE_VERSION=latest
LABEL description="Solarus Linux build environment image" \
      maintainer="The Solarus Team" \
      version="${IMAGE_VERSION}" \
      org.solarus-games.pkglist-cmd="pacman -Q"

# Synchronise pacman repositories and update system using yay
RUN sudo -u yay -- yay --noconfirm -Syu

# Install required packages using yay
RUN sudo -u yay -- yay --noconfirm --needed -S \
    cmake pkg-config \
    sdl2 sdl2_image sdl2_ttf \
    openal libvorbis libmodplug \
    glm physfs lua51 luajit-2.1 \
    qt5-base qt5-tools \
    xorg-server-xvfb

# Clear yay cache
RUN sudo -u yay -- rm -rf /home/yay/.cache/*

# Remove unnecessary data from the image
RUN rm -rf /usr/share/{doc,gtk-doc,man,info,texinfo,locale,i18n,guile,ibus,perl5}/*
