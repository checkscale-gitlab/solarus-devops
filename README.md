# solarus-devops

DevOps for Solarus projects.
This repository includes Docker images for building, testing and packaging
Solarus components, as well as for generating documentation.
Also it includes GitLab CI/CD configurations that leverage these images.

## Docker Images

All the Docker images in this repository are available in
[Docker Hub](https://hub.docker.com/u/solarus).

Below are instructions for building the Docker images.
If your Docker installation supports experimental features, it is recommended
that you build the images using the `--squash` option.

See: https://docs.docker.com/engine/reference/commandline/image_build/#options

### Environment Images for Building

Base build environment image:

    docker build -t solarus/base-build-env base-build-env

Linux-based build environment image:

    docker build -t solarus/linux-build-env linux-build-env

MinGW-based build environment image:

    docker build -t solarus/mingw-build-env mingw-build-env

Documentation build environment image:

    docker build -t solarus/docs-build-env docs-build-env

### Utilities Environment Image

    docker build -t solarus/utilities-env utilities-env

### Pushing Images to Docker Hub

Login to Docker Hub:

    docker login

Push images after building:

    docker push solarus/base-build-env
    docker push solarus/linux-build-env
    docker push solarus/mingw-build-env
    docker push solarus/docs-build-env
    docker push solarus/utilities-env

## GitLab CI/CD Configurations

Currently there are four GitLab CI/CD configurations for Solarus projects:

* `solarus.yml`: for building, testing and packaging the Solarus Engine.
* `solarus-quest-editor.yml`: for building and packaging the Solarus Quest Editor.
* `solarus-doc.yml`: for building the Solarus documentation.
* `quests.yml`: for packaging Solarus quests.

### Solarus CI/CD

The Solarus CI/CD is able to build, test and package the Solarus Engine.

A summary of the current main features is below:

* Separate stages: `build`, `test` and `package`.
* Coverage report and coverage badge support for tests.
* Packages for Win32, Win64 and Win64-Portable (experimental).
* For non-releases (commits): pipelines only run for Linux and Win64.
* For releases (tags): pipelines run for all platforms.
* The generated packages can be downloaded directly from GitLab.
* Pipelines only execute when relevant files are changed, i.e. source code.

At the moment, Windows-based testing is not available.

More details can be found in the relevant YAML configuration file.

### Solarus Quest Editor CI/CD

The Solarus Quest Editor CI/CD is able to build and package the Solarus Quest
Editor including a copy of the Solarus Engine and the Solarus Sample Quest.

A summary of the current main features is below:

* Separate stages: `build`, `install`, `package`
* Packages for Win32 and Win64.
* For non-releases (commits): pipelines only run for Linux and Win64.
* For releases (tags): pipelines run for all platforms.
* The generated packages can be downloaded directly from GitLab.
* Pipelines only execute when relevant files are changed, i.e. source code.

More details can be found in the relevant YAML configuration file.

### Solarus Documentation CI/CD

The Solarus documentation CI/CD builds the Doxygen documentation of Solarus.

The current pipeline builds the documentation in HTML and Latex+PDF formats.

### Quests CI/CD

Quest makers can have their quests in GitLab automatically tested and packaged
into `.solarus` files simply by using the following `.gitlab-ci.yml` file:

```yml
include:
  - https://gitlab.com/solarus-games/solarus-devops/raw/master/gitlab/quests.yml
```

The quests pipeline can be customised by using the
[GitLab CI/CD Variables UI](https://docs.gitlab.com/ee/ci/variables/#via-the-ui):

* `SKIP_TESTING` set to any value will skip test jobs.
* `SKIP_PACKAGING` set to any value will skip package jobs.
* `SOLARUS_REF` (defaults to `dev`) is the Git ref to use for downloading
  the Solarus Engine artifact containing the testing tools.

For testing, quest makers must define test Lua maps in `data/maps/tests` (or a
subdirectory) that will be run automatically by the `quest-test` job in the
pipeline. Only map files beginning with the prefix `test_` will be run. Each
test map script should call `sol.main.exit()` once the test is complete.

The quests pipeline will build the following quest packages:

* For non-releases (commits): `<project-name>-<commit-hash>.solarus`
* For releases (tags): `<project-name>-<tag-name>.solarus`

Alternatively, quest makers can also copy the complete `quests.yml`
file directly into their repositories and name it `.gitlab-ci.yml`.

More details can be found in the relevant YAML configuration file.

### Artifacts Download

Generated artifacts can be downloaded using the following URL template:

    https://gitlab.com/solarus-games/<project>/-/jobs/
        artifacts/<ref>/raw/<path_to_file>?job=<job_name>

Where `<project>` is a Solarus project with an enabled pipeline, `<ref>` is
a Git reference such as a branch name or a tag, `<path_to_file>` is the
artifact file to download and `<job_name>` is the pipeline job name that
generated the desired artifact, e.g. typically a package job.

### Pipeline Badges

Pipeline status and test coverage badges can be displayed using the
following URL templates:

    https://gitlab.com/solarus-games/<project>/badges/<branch>/pipeline.svg
    https://gitlab.com/solarus-games/<project>/badges/<branch>/coverage.svg

Where `<project>` is a Solarus project with an enabled pipeline and
`<branch>` is the desired branch to display.

Project badges can also be added in **Settings -> General -> Badges**.
